import React, {useState} from 'react';
import {
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const DetailScreen = ({navigation}) => {
  const [title, setTitle] = useState('');
  const [tags, setTags] = useState('');

  const save = async () => {
    let formData = {
      title,
      tags,
      // time: Date.now(),
    };

    // console.log(formData);

    // const PushTodos = [...]

    try {
      await AsyncStorage.setItem('todos', JSON.stringify(formData));
      navigation.navigate('Home');
    } catch {}
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={{backgroundColor: '#303030', flex: 1, padding: 30}}>
        <View style={{paddingTop: 80, paddingLeft: 30, paddingRight: 30}}>
          <Text style={{color: 'yellow'}}>To-do</Text>
          <TextInput
            placeholderTextColor="white"
            placeholder="Thêm thông tin"
            style={{color: 'white'}}
            multiline={true}
            onChangeText={value => setTitle(value)}
          />
        </View>

        <View style={{paddingTop: 20, paddingLeft: 30}}>
          <Text style={{color: 'yellow'}}>Tags</Text>
          <TextInput
            placeholderTextColor="white"
            placeholder="Thêm thông tin"
            style={{color: 'white'}}
            multiline={true}
            onChangeText={value => setTags(value)}
          />
        </View>

        <TouchableOpacity
          style={{
            width: '100%',
            backgroundColor: 'yellow',
            padding: 15,
            marginTop: 35,
            borderRadius: 10,
            bottom: 20,
            right: 30,
            position: 'absolute',
          }}>
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontWeight: 'bold',
              fontSize: 15,
            }}
            onPress={() => save()}>
            Save
          </Text>
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default DetailScreen;
