import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Button,
  TouchableOpacity,
  StyleSheet,
  FlatList,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const HomeScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: 'yellow'}}>
      <View style={{padding: 60, marginTop: 50}}>
        <Text style={{fontSize: 25, fontWeight: 'bold'}}>Buy Food</Text>
        <Text>#Home</Text>
      </View>

      <TouchableOpacity
        style={styles.btnadd}
        onPress={() => navigation.navigate('Detail')}>
        <Text style={styles.add}>+</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  btnadd: {
    width: 70,
    height: 70,
    backgroundColor: 'black',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 30,
    right: 30,
    position: 'absolute',
  },
  add: {
    color: 'white',
    fontSize: 25,
  },
});
export default HomeScreen;
